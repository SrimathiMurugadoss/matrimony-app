import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profile: any;
  image = 50;
  profiles: any;

  constructor(private service: ServiceService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile(): void {
    this.service.getProfile().subscribe((res: any) => {
      this.profiles = res;
      this.profile = res.list[0];
    }, error => {
      throw error;
    });
  }

  interest(id: any, name: string): any {
    this.image++;
    this.toastr.success(`Interested in this profile ${name}`);
    this.profiles.list = this.profiles.list.filter((a: any) => a.id != id);
    if (this.profiles.list.length === 0) {
      this.getProfile();
    } else {
      this.profile = this.profiles.list[0];
    }
  }

  reject(id: any, name: string): any {
    this.image++;
    this.toastr.warning(`Not Interested in this profile ${name}`);
    this.profiles.list = this.profiles.list.filter((a: any) => a.id != id);
    if (this.profiles.list.length === 0) {
      this.getProfile();
    } else {
      this.profile = this.profiles.list[0];
    }
  }

}
